<!DOCTYPE html>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<html>
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=Edge">
<sj:head jqueryui="true" ajaxcache="false" compressed="false"
	jquerytheme="cupertino" />
<title><s:text name="pts.project.title" /></title>
<link rel="stylesheet" type="text/css"
	href="<%=request.getContextPath()%>/styles/pts.css" />
<script src="<%=request.getContextPath()%>/js/pts.js"
	type="text/javascript"></script>
<script type="text/javascript"
	src="<%=request.getContextPath()%>/struts/js/base/jquery.ui.datepicker.js"></script>
<script type="text/javascript"
	src="<%=request.getContextPath()%>/js/jszip.js"></script>
<script type="text/javascript"
	src="<%=request.getContextPath()%>/js/FileSaver.js"></script>
<script type="text/javascript"
	src="<%=request.getContextPath()%>/js/myexcel.js"></script>
<script>

	var totalStoryPoints=50;
	var totalHoursPerPi=8;
	var projectMap={};
	var releaseMap={};
	var stableTeamsMap={};
	var ids=[];
	
	function getFromNw(obj,method,action,parseObjName){
		 document.getElementById("loadingIndicator").style.display="block"
		const requestOptions = {
		    method: method,
		    headers: { 'Content-Type': 'application/json' },
		    body: JSON.stringify(obj)
		};
		document.getElementById("loadingIndicator").style.display="block";
		fetch(location.protocol+"//"+location.host+"/"+location.pathname.split('/')[1]+"/"+action+"", requestOptions)
		    .then(async response => {
		        const isJson = response.headers.get('content-type')?.includes('application/json');
		        const data = isJson && await response.json();
		        if (!response.ok) {
		            const error = (data && data.message) || response.status;
		            return Promise.reject(error);
		        }
		        if(action == 'projects/addUserNetworkContributionAjax.action'){
		        	alert("Records Saved")
		        }
		         switch (parseObjName) {
				case 'releasesMapObj':
					 if(data!=null && data.releasesMapObj!=null){
							releaseMap= data.releasesMapObj ;
						 	Object.values(data.releasesMapObj).map(elem=> $("#releases").append("<option value='"+elem+"'>"+elem+"</option>"));
						} 
					break;
				case 'getUserNwCapacity':
						generatetable(data);
						checkAndLoadStableTeams();
					break;
				}  
		          
		    })
		    .catch(error => {
		    	alert("Something went wrong please try after sometime or please raised a feedback ticket"
		    			+location.protocol+"//"+location.host+"/"+location.pathname.split('/')[1]+"/others/goGetfeedback.action")
		        console.error(  error);
		    });
		
		 document.getElementById("loadingIndicator").style.display="none"
	}
	
	
	function showAllResourcesClick() {
		document.forms[0].method = "post";
		document.forms[0].action = "../projects/assignResources.action";
		document.forms[0].submit();
	}
	function loadProjects() {
		totalStoryPoints=parseFloat(document.getElementById("totalStoryPoints").value);
		totalHoursPerPi=parseFloat(document.getElementById("totalHoursPerPi").value) ;
		document.getElementById("loadingIndicator").style.display="block"
			$("#release").val('')
		var obj = {
				'project' : $("#project").val(),
				'showUserNwCapacity':"No"
		};
		var jqxhr = $.post('../utilization/getProjectsMapAjax.action', (obj),
				function(res) {
					if(res!=null && res.projectsMapObj!=null){
						projectMap= new Map(Object.entries(res.projectsMapObj)); ;
						Object.values(res.projectsMapObj).map(elem =>
						$("#projects").append("<option value='"+elem+"'>"));
					} 
					
					loadStableTeams(res);
					
				}).fail(function(err) {
				 console.log(err)
		});		
		document.getElementById("loadingIndicator").style.display="none"
	}
	function loadStableTeams(res){
		if(res!=null && res.stableTeamsMapobj!=null){
			stableTeamsMap= new Map(Object.entries(res.stableTeamsMapobj)); 
			let mapIter = stableTeamsMap.entries();
			
			while(true){ 
				if(mapIter.next().done){
					break;
					}
				else {
						let val = mapIter.next().value;
						$("#stableTeams").append("<option  key='"+val[0]+"' value='"+val[1]+"'> </option>")	
					}   
				}
		} 
	}
	function loadRelease(loc) {	
		$("#releases").empty();
		if(loc=='P'){
			 $("#stableTeam").val('')
		}else if(loc=='S'){
			 $("#project").val('')
		}
		if($("#project").val() !== '' || (  $("#stableTeam").val() !== 'All' &&  $("#stableTeam").val() !== '')){
			$("#release").val('')
			var obj = {
					'project' : $("#project").val(),
					'stableTeamName' : $("#stableTeam").val(),
					'stableTeam': $("#stableTeam").val(),
					'showUserNwCapacity':"No"
			};
		  getFromNw(obj,'POST','utilization/getReleaseMapAjax.action','releasesMapObj');
	    
		}
	}
	
	function getUserNwCapacity(){
		$("#contributionNW").empty();
		var obj = {
				'project' : $("#project").val(),
				'release' : $("#releases").val(),
				'stableType':$("#addStableToNwContribution").val(),
				'stableTeamName' : $("#stableTeam").val(),
				'userBasedFilter' :( $("#userBasedFilter").val()==null)? $("#userBasedFilter").val():'All',
				'showUserNwCapacity':"Yes"
			};	
		let res = getFromNw(obj,'POST','projects/manageUserNetworkContributionAjax.action','getUserNwCapacity');
		 
	}
	
	function checkAndLoadStableTeams(){
		if(document.getElementById("addStableToNwContribution").checked){
			stableIds = document.querySelectorAll('[id ^= "stableContrib_"]');
			Array.prototype.forEach.call(stableIds, callback);
			if(ids.length > 0){
				ids.forEach(data => {
					document.getElementById("ncStableContrib_"+data).value =document.getElementById("stableContrib_"+data).value;
					document.getElementById("diffContribution_"+data).value	= parseFloat(document.getElementById("stableContrib_"+data).value) *totalStoryPoints /100;
				})
			}
		}
	}
	function saveUserNwContribution(){
		var nwList = [];
		$("#grid2 tbody tr").each(function() {
			var tmpId=$(this).find('td').find('input').attr('id').split("_")[1];
			var networkGridModel={ 
					"id":$("#id_"+tmpId).val(),
					"nwId":$("#nwId_"+tmpId).val(),
					"nccStableContribution":$("#ncStableContrib_"+tmpId).val(),
					"stableContribution":$("#stableContrib_"+tmpId).val(),
					"pmStableContribution":$("#pmStableContrib_"+tmpId).val()
				};
				
			nwList.push(networkGridModel)
			});
	
		var ajaxData = {
				"totalHoursPerPi":parseFloat(document.getElementById("totalHoursPerPi").value),
				"totalStoryPoints":parseFloat(document.getElementById("totalStoryPoints").value),
				"assignDefNeContribution":document.getElementById("addStableToNwContribution").checked
				};
	    ajaxData["networkGridModel"] = nwList; 
	    
	 	getFromNw( ajaxData,'POST','projects/addUserNetworkContributionAjax.action','getUserNwCapacity');
	}
	
	function downloadUserNwContribution(){
		 downloadExcel('')
	}
	function filterByUserType(type){
		$("#grid2 tbody tr").each(function() {        
	        	var tdId=$(this).find('td').attr('id').split("_")[1]; 
				 switch(type.value){
					case 'On Track':
						if(checkForValueType(parseFloat($('#userRemainEffort_'+tdId).val()))<0){
							$("#tr_"+tdId).css('display','none')
						}else{
						$("#tr	_"+tdId).css('display','table-row')
						}
					break;
					case 'Off Track':
						if(checkForValueType(parseFloat($('#userRemainEffort_'+tdId).val()))>=0){
							$("#tr_"+tdId).css('display','none')
						}else{
						$("#tr_"+tdId).css('display','table-row')
						}
					break;
					default:
						$("#tr_"+tdId).css('display','table-row')
					break;
				 }			 
	        });	
		}
		function checkForValueType(val){
			if(val >=0){
				return 1;
			}else if(val <= 0){
				return -1;
			}else{
				return 0;
			}	
		}
	var networkGridModel={};
	function generatetable(res){
		if(res!=null && res.networkGridModel!=null){
			if(res.networkGridModel.length <=0){
				alert('No Users assigned for this release')
			}else{
				
				networkGridModel=res;
				document.getElementById("projectDiv").style.display="none";
				document.getElementById("releaseHeader").style.display="block";
				document.getElementById("gridDiv").style.display="grid";
			
		 
			document.getElementById("totalStoryPoints").value=totalStoryPoints;
			document.getElementById("releaseHeader").innerHTML='Release:' +res.networkGridModel[0].networkCode;
			
			Object.values(res.networkGridModel).map(elem=>{
				 	$("#contributionNW").append(
				 			"<tr id='tr_"+elem.id+"'>"+
				 			"<td id='td_"+elem.id+"'><input id='id_"+elem.id+"' type='hidden' value='"+elem.id+"' /> "+
				 			"<input id='name_"+elem.id+"' readonly='readonly' type='text' style='text-align: center;border: none;' value='"+elem.name+"' /></td> "+
				 			"<td style='display:none;''><input id='nwId_"+elem.id+"' type='hidden' value='"+elem.nwId+"' /> "+
				 			"<input  id='networkCode_"+elem.id+"' readonly='readonly' type='text' style='text-align: center;border: none;' value='"+elem.networkCode+"'/></td>"+
				 			"<td><input onChange='updateStableContribution(this)' id='ncStableContrib_"+elem.id+"' type='text' style='width:47px !important;text-align: center;' value='"+elem.nccStableContribution+"'></input></td>"+
				 			"<td><input id='diffContribution_"+elem.id+"' disabled type='text' style='width:47px !important;text-align: center;' value='"+((elem.nccStableContribution*totalStoryPoints)/100)+"'></input></td>"+
				 			"<td><input id='stableContrib_"+elem.id+"'  type='text' style='display:none;' value='"+elem.stableContribution+"'></input>"+
				 			"<input onChange='updateStableContribution(this)' id='pmStableContrib_"+elem.id+"' type='text' style='width:47px !important;text-align: center;' value='"+elem.pmStableContribution+"'></input></td>"+
				 			"<td><input id='userContrib_"+elem.id+"' disabled type='text' style='width:47px !important;text-align: center;' value='"+((((elem.nccStableContribution*totalStoryPoints)/100)) -elem.pmStableContribution )+"'></input></td>"+
				 			"<td><input id='availableHours_"+elem.id+"' disabled type='text' style='width:47px !important;text-align: center;' value='"+(parseFloat(elem.pmStableContribution) * totalHoursPerPi)+"'></input></td>"+
				 			"<td><input id='userEffort_"+elem.id+"' disabled type='text' style='width:47px !important;text-align: center;' value='"+(elem.userEffort )+"'></input></td>"+
				 			"<td><input id='userRemainEffort_"+elem.id+"' disabled type='text' style='width:47px !important;text-align: center;' value='"+( (parseFloat(elem.pmStableContribution)*parseFloat(totalHoursPerPi)) - elem.userEffort)+"'></input></td>"+
				 			"</tr>"
				 			);  
				 	});
			
		 	}
		}
		checkAndLoadStableTeams();
	}

	function downloadExcel(data){
		   var excel = $JExcel.new("Calibri black 11 #000000");	
			
			excel.set( {sheet:0, value:"User Contributions" } );
		    excel.addSheet("Sheet 2");
			
			
			/*excel.set(0,8,1,15);		
			excel.set(0,8,2,13);		
			excel.set(0,7,3,"15+13");		
			excel.set(0,8,3,"=I2+I3");		

			
			var evenRow=excel.addStyle ( {
				border: "none,none,none,thin #333333"});

			var oddRow=excel.addStyle ( {
				fill: "#ECECEC" ,
				border: "none,none,none,thin #333333"}); 
			*/
			/*
			for (var i=1;i<50;i++) excel.set({row:i,style: i%2==0 ? evenRow: oddRow  });
			excel.set({row:3,value: 30  });
*/
			var headers=[
				"PI Name",
				"ResourceName",
				"Allocated",
				"Blocked and Balance",
				"Consumed Hours",
				"Balance Hours",
				"Clocked Resource Hours",
				"Reamining Resource Hours",
				];
			var formatHeader=excel.addStyle ( {
					fill: "#ECECEC" ,
					border: "thin #000000",
					font: "Calibri 12 #000000 B"});

			for (var i=0;i<headers.length;i++){
				excel.set(0,i,0,headers[i],formatHeader);
				excel.set(0,i,undefined,"auto");
			}
					
			// Now let's write some data
			var initDate = new Date(2000, 0, 1);
			var endDate = new Date(2016, 0, 1);
			var dateStyle = excel.addStyle ( { 
					align: "R",
					format: "yyyy.mm.dd hh:mm:ss",
					font: "#00AA00"});
			i=1;
			$("#grid2 tbody tr").each(function() {  
						var tmpId=this.id.split("_")[1];
						excel.set(0,0,i,$('#releaseHeader').val());
						excel.set(0,1,i,$('#name_'+tmpId).val());
						excel.set(0,2,i,$('#ncStableContrib_'+tmpId).val());
						excel.set(0,3,i,$('#diffContribution_'+tmpId).val());
						excel.set(0,4,i,$('#pmStableContrib_'+tmpId).val());
						excel.set(0,5,i,$('#userContrib_'+tmpId).val());
						excel.set(0,6,i,$('#userEffort_'+tmpId).val());
						excel.set(0,7,i,$('#userRemainEffort_'+tmpId).val());
						i++;
				});
	 
 		 excel.generate("UserContribution.xlx");
	}
	
	function showProjectData(){
		document.getElementById("projectDiv").style.display="table";
		document.getElementById("releaseHeader").style.display="none";
		document.getElementById("gridDiv").style.display="none";
	}
	function addStableToNwContribution(){
			 getUserNwCapacity();
	}
	function callback(element, iterator) {
		ids.push( element.id.split("_")[1]);
		 }
	function updateStableContribution(ref){
		var id=ref.id.split("_")[1];
		console.log(id, ' -  ' ,totalStoryPoints, ' -  ' ,document.getElementById("pmStableContrib_"+id ).value );
//((elem.nccStableContribution*totalStoryPoints)/100)
		//$("#diffContribution_"+id ).val((parseFloat($("#ncStableContrib_"+id ).val() + totalStoryPoints) / 100));
document.getElementById("diffContribution_"+id ).value = totalStoryPoints * document.getElementById("ncStableContrib_"+id ).value  /100;

document.getElementById("userContrib_"+id ).value=
			 (parseFloat(document.getElementById("diffContribution_"+id ).value ) - parseFloat(document.getElementById("pmStableContrib_"+id ).value))  ;
document.getElementById("availableHours_"+id ).value=parseFloat(document.getElementById("pmStableContrib_"+id).value)* parseFloat(totalHoursPerPi);
		 
		document.getElementById("userRemainEffort_"+id ).value=	(parseFloat(document.getElementById("pmStableContrib_"+id).value)* parseFloat(totalHoursPerPi))  - parseFloat( document.getElementById("userEffort_"+id ).value);
			
	}
	function userBasedFilter(){
		
		
		
	}
	function changeTotalHoursPerPi(){
		totalHoursPerPi=parseFloat(document.getElementById("totalHoursPerPi").value);
		var nwList = [];
		$("#grid2 tbody tr").each(function() {
			var tmpId=$(this).find('td').find('input').attr('id').split("_")[1];
			var networkGridModel={ 
					"id":$("#id_"+tmpId).val(),
					"nwId":$("#nwId_"+tmpId).val(),
					"nccStableContribution":$("#ncStableContrib_"+tmpId).val(),
					"stableContribution":$("#stableContrib_"+tmpId).val(),
					"pmStableContribution":$("#pmStableContrib_"+tmpId).val()
				};
				
			nwList.push(networkGridModel)
			});
		networkGridModel.networkGridModel=nwList
		 $("#contributionNW").empty();
		 generatetable(networkGridModel)
	}
</script>
</head>
<body onload="loadProjects();">
	<div method="POST" id="manageNetworkCodeFormId">
		<jsp:include page="../header.jsp" />
		<jsp:include page="../menu.jsp" />
		<div id="breadcrumbDivParent" style="width: 100%; height: 22px;">
			<div
				style="margin: 0 auto; width: 1000px; background: url('../images/breadcrum_bg.png'); height: 22px; text-align: left; line-height: 22px;">
				<div id="breadcrumbDiv" style="margin-left: 25px; clear: both;">
					<s:a href="../login/showDashboard.action">
						<s:text name="pts.menu.home" />
					</s:a>
					>
					<s:a href="../projects/manageUserNetworkContribution.action">
						<s:text name="Project Utilization" />
					</s:a>
				</div>
			</div>
		</div>
		<s:hidden name="id" id="ncId" />
		<div id="contentarea">
			<DIV style="margin: 0 auto; width: 980px;">
				<div class="titleContainer">
					<DIV id=spacer5px>&nbsp;</DIV>
					<s:div cssClass="pagetitle">
						<s:text name="Project Utilization" />
					</s:div>
				</div>

				<div id="spacer10px">&nbsp;</div>
				<div id="spacer10px">&nbsp;</div>
				<div>
					<p id="releaseHeader"
						style="display: none; margin-right: 1px; font-size: 16px; font-weight: bolder;">
						Release:</p>
				</div>
				<div>
					<table id="projectDiv" style="margin: auto;">
						<tr>
							<td style="color: #2779aa;" align="right">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<s:text
									name="pts.networkcode.pillar.name" />:
							</td>
							<td width="600"><input list="projects" name="project"
								style="width: 600px; font-size: 15px;"
								onchange="loadRelease('P')" id="project" /> <datalist
									id="projects">
								</datalist></td>
						</tr>
						<tr>
							<td style="color: #2779aa;" align="right">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<s:text
									name="Stable Team" />:
							</td>
							<td width="600"><input list="stableTeams" name="stableTeam"
								style="width: 600px; font-size: 15px;"
								onchange="loadRelease('S')" id="stableTeam" /> <datalist
									id="stableTeams">
								</datalist></td>
						</tr>

						<tr>
							<td style="color: #2779aa;" align="right">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<s:text
									name="pts.netwok.code" />:
							</td>

							<td width="600"><select name="release" id="releases"
								style="width: 100%; font-size: 15px;" size="15">

							</select></td>

							<td><img id="loadingIndicator"
								src="../images/indicatorImg.gif" style="display: none" /></td>
							<td style="padding-left: 20px;"><img src="../images/go.png"
								onclick="getUserNwCapacity()" /></td>
						</tr>
					</table>
				</div>
				<p style="color: red;" id="message"></p>
				<div id="spacer10px">&nbsp;</div>
				<div id="gridDiv" style="display: none;">
					<table>
						<tbody>
							<tr>
								<td style="width: 30%;"><label for="totalStoryPoints">Total
										Story Points</label> <input type="text" value="50"
									name="totalStoryPoints" id="totalStoryPoints" /></td>
								<td style="width: 40%;"><label for="totalHoursPerPi">Productive
										hours for this PI Points</label> <input type="text" value="8"
									name="totalHoursPerPi" onChange="changeTotalHoursPerPi()" id="totalHoursPerPi" /></td>

								<td style="width: 20%;"><label for="userBasedFilter">Filter
								</label> <select name="userBasedFilter" id="userBasedFilter"
									onchange="filterByUserType(this)"
									style="  float: right; margin-right: 34px;">
										<option value="All">All</option>
										<option value="On Track">On Track</option>
										<option value="Off Track">Off Track</option>
								</select></td>
							</tr>
							<tr>
								<td></td>
								<td></td>
								<td style="width: 100%;"><p style="margin-right: 11%;">
										Clone Contribution From <select
											style="float: right; margin-right: 4px;"
											onchange="addStableToNwContribution()" type="checkbox"
											id="addStableToNwContribution">
											<option value="-1">Please Select</option>
											<option value="1">Previous PI</option>
											<option value="2">Resource Wise</option>
										</select>
									</p></td>
							</tr>
						</tbody>
					</table>
					<div
						style="border-bottom: solid 1px black; overflow-y: auto; height: 400px;">
						<div id="spacer1px">&nbsp;</div>
						<table id="grid2"
							style="width: 95%; margin: auto; text-align: center; border: 1px solid">
							<thead>
								<tr style="background-color: #cdcdcd;">
									<th>Resource</th>
									<th>Contribution</th>
									<th>Contribution based<br /> on story points
									</th>
									<th>Committed Story<br /> Points
									</th>
									<th>Un-utilized<br /> Contribution</th>
									<th>Available Hours</th>
									<th>Charged Hours</th>
									<th>Balance Hours</th>

								</tr>
							</thead>
							<tbody id="contributionNW">
							</tbody>
							<tfoot>
							</tfoot>
						</table>
					</div>
					<div style="margin-top: 1%;">
						<button
							style="float: left; padding: 3px; margin-left: 2px; background-color: #87C1FF; color: black; font-weight: bold; font-size: 13px;"
							onclick="showProjectData()">Go Back</button>
						<div style="float: right;">
							<button
								style="padding: 3px; margin-right: 2px; background-color: #87C1FF; color: black; font-weight: bold; font-size: 13px;"
								onclick="downloadUserNwContribution()">Download</button>
							<button
								style="padding: 3px; margin-right: 2px; background-color: #87C1FF; color: black; font-weight: bold; font-size: 13px;"
								onclick="saveUserNwContribution()">Submit</button>
						</div>
					</div>

				</div>
				<s:div id="spacer10px">&nbsp;</s:div>
				<s:div id="spacer10px">&nbsp;</s:div>
			</div>
			<s:include value="/jsp/footer.jsp"></s:include>
		</div>
	</div>
</body>
</html>